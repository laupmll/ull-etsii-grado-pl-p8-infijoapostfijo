/* description: Parses end executes mathematical expressions. */

/* lexical grammar */
/* operator associations and precedence */

%lex
%%

\s+                   /* skip whitespace */
[0-9]+("."[0-9]+)?\b  return 'NUMBER'
"*"                   return '*'
"/"                   return '/'
"-"                   return '-'
"+"                   return '+'
"^"                   return '^'
"!"                   return '!'
"%"                   return '%'
"("                   return '('
")"                   return ')'


"="                   return '='
";"                   return ';'
\b[a-zA-Z_]\w*\b	return idORrw(yytext)

[<>]			return yytext
[<>=!]"="		return yytext


<<EOF>>               return 'EOF'
.                     return 'INVALID'

/lex



%right '='
%left '+' '-'
%left '*' '/'
%left '^'
%right '!'
%right '%'
%left UMINUS

%start expressions

%{
	var reserved_words={PI: 'PI', E: 'E'};
	function idORrw (x){
		return ((x.toUpperCase() in reserved_words)? x.toUpperCase() : 'ID');
	}
	

	var symbol_table = {};
	var array = function(hash){
		var vector = [];
		for (var k in hash){
			vector.push(hash[k]);
		}
		return vector;
	}
		

	var make_traverse = function(){
		var seen = [];
		return function(key, val) {
			if (typeof val == "object"){
				if (seen.indexOf(val)>=0){
					return undefined;
					seen.push(val);
				}
				return val};
		}
	};
	
%}

%% /* language grammar */

