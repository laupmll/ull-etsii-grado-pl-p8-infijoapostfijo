/* description: Parses end executes mathematical expressions. */

/* lexical grammar */
%lex
%%

\s+                   /* skip whitespace */
[0-9]+("."[0-9]+)?\b  return 'NUMBER'
";"                   return ';'
\b[a-zA-Z_]\w*\b	return idORrw(yytext)

[=!]"=="		return 'OPERATOR3'
[<>=!]"="		return 'OPERATOR2'
[-+*/^%()<>=!]		return yytext

<<EOF>>               return 'EOF'
.                     return 'INVALID'

/lex

/* operator associations and precedence */


%right '=' '<' '>'
%left '<=' '>=' '==' '!='
%left 'OPERATOR2'
%left 'OPERATOR3'
%left '+' '-'
%left '*' '/'
%left '^'
%right '!'
%right '%'
%right EL
%left ELSE
%left UMINUS

%start expressions

%{
	var reserved_words = {IF: 'IF', THEN: 'THEN', ELSE: 'ELSE', PI: 'PI', E: 'E'};
	function idORrw (x) {	
	return ((x.toUpperCase() in reserved_words)? x.toUpperCase() : 'ID');
	}
	
	/* if x then y */
	/*$$=[]; $$.push($2); $$.push('jmpz'); $$.push($4); $$.push(':endif');*/
	function translateIf (x, y){
		var pila = [];
		pila.push(x);
		//pila.push('jmpz');
		pila.push('jmp');
		pila.push(y);
		//pila.push(':endif');
		return (pila);
	}
	
    	/*IF x THEN y ELSE z */
	/*$$=[]; $$.push($2); $$.push('jmpz'); $$.push($4); $$.push('jmp'); 
	$$.push(':else'); $$.push($6); $$.push(':endif');*/
	function translateIfElse (x, y, z){
		var pila = translateIf(x, y);
		//alert('traslateIf'+pila);
		pila.push('jmp');
		pila.push(':else');
		pila.push(z);
		pila.push(':endif');
		return pila;
	}	


	var symbol_table = {};
	var array = function(hash){
		var vector = [];
		for (var k in hash){
			vector.push(hash[k]);
		}
		return vector;
	}
		

	var make_traverse = function(){
		var seen = [];
		return function(key, val) {
			if (typeof val == "object"){
				if (seen.indexOf(val)>=0){
					return undefined;
					seen.push(val);
				}
				return val};
		}
	};
	
%}

%% /* language grammar */

expressions
    : s EOF
        { 
	  typeof console !== 'undefined' ? console.log($1) : print($1);
	//$$=[];
	//$$.push(5);
return ('<br>'+$1.toString()).split(',').join('<br>'); //split pasa a vector
//return '<br>'+$1.join('<br>');  
//return '<ol>\n'+$1+'</ol>';  
	//return $1;
	}
    ;

s   : s ';' c	{$$= $1; $$.push($3);}	/*Permite varias sentencias*/
    | c		{$$ = $1;}
    ;

c   :		{$$ = [];}	/*Permite que hayan sentencias vacías*/ 
    | e		{$$ = $1;}
    | IF e THEN c	%prec EL	
	{$$ = translateIf($2, $4); $$.push(':endif');   
	/*$$=[]; $$.push($2);$$.push('jmpz'); $$.push($4); $$.push(':endif');*/}
    | IF e THEN c ELSE c
	{$$ = translateIfElse($2, $4, $6);
	/*$$ = []; $$.push($2); $$.push('jmpz'); $$.push($4); $$.push('jmp'); $$.push(':else'); $$.push($6); $$.push(':endif');*/}   
    ;


e
    : ID '=' e
	{ $$=[]; $$.push($3); $$.push($1); $$.push($2);}
    | ID
	{$$ = $1; }
    | e '+' e
        { $$=[]; $$.push($1); $$.push($3); $$.push($2); }
    | e '-' e
        {$$ = []; $$.push($1); $$.push($3); $$.push($2);}
    | e '*' e
        {$$ = []; $$.push($1); $$.push($3); $$.push($2);}
    | e '/' e
        {$$ = []; $$.push($1); $$.push($3); $$.push($2);
	if ($3 === 0){ throw new Error('Division by zero');}}
    | e '^' e
        {$$ = []; $$.push($1); $$.push($3); $$.push($2);}
    | '!' e
        {$$ = []; $$.push($1); $$.push($2);}
    | e '%' e
        {$$ = []; $$.push($1); $$.push($3); $$.push($2);}
    | '-' e %prec UMINUS
        {$$ = []; $$.push($2); $$.push($1);}
    | '(' e ')'
        {$$ = $2;}
    | NUMBER
        {$$ = Number(yytext);}
    | E
        {$$ = Math.E;}
    | PI
        {$$ = Math.PI;}
    | E '=' e
	{throw new Error('E can not be assigned'); }
    | PI '=' e
	{throw new Error('PI can not be assigned'); }
    | e 'OPERATOR2' e
        {/*alert("yytext: "+$2);*/
	$$ = []; $$.push($1); $$.push($3); $$.push($2);}
    | e 'OPERATOR3' e
        {$$ = []; $$.push($1); $$.push($3); $$.push($2);}
    | e '<' e 
        {$$ = []; $$.push($1); $$.push($3); $$.push($2);}
    | e '>' e 
        {$$ = []; $$.push($1); $$.push($3); $$.push($2);}
    ;

